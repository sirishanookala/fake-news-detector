# Fake News Detector

This is an easy-to-use web application that helps online readers to  recognize the fake news in less than a minute with an accuracy of 90% , all with just an internet connection!

This has been implemented on various machine learning algorithms and pre-processing techniques to find the right fit including:
1) Logistic Regression
2) KNN
3) ANN
4) SVM
5) LSTM
6) BERT 
7) DistilBERT

It has been found that the implementation of LSTM or DistilBERT with a combination of Logistic Regression have produced the better results compared to the others. A minimalistic UI has also been created and has been hosted on [**Fake News Detector**](http://3.23.59.214/). The hosting has been done on an AWS EC2 instance to make it public.

All of this has been done with an amazing cross-functional team of 6 graduate students who have been co-ordinatinating by using [**JIRA**](https://sirishanookala.atlassian.net/jira/software/projects/FND) as a product management platform. 

**Note:** The instance has been terminated because the free tier limit of AWS has been exceeded.